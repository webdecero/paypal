<?php

namespace Webdecero\Paypal\Pages\Traits;

//Models
//Helpers and Class
use Illuminate\Http\Request;
//Context Paypal
//Unique transaccion
use PayPal\Api\Payer;
//use PayPal\Api\ChargeModel;
use PayPal\Api\Currency;
use PayPal\Api\MerchantPreferences;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\Plan;
use PayPal\Api\Agreement;
//use PayPal\Api\ShippingAddress;
use PayPal\Api\Patch;
use PayPal\Api\PatchRequest;
use PayPal\Common\PayPalModel;

trait PaypalBillingPlans {

    private function planProcess($resource, $routeReturnUrl, $routeCancelUrl) {


        $plan = new Plan();

        $plan->setName($resource['title'])
                ->setDescription($resource['description'])
                ->setType('INFINITE');


        $paymentDefinition = new PaymentDefinition();
// The possible values for such setters are mentioned in the setter method documentation.
// Just open the class file. e.g. lib/PayPal/Api/PaymentDefinition.php and look for setFrequency method.
// You should be able to see the acceptable values in the comments.
        $paymentDefinition->setName($resource['description'])
                ->setType('REGULAR')
                ->setFrequency($resource['frequency'])
                ->setFrequencyInterval($resource['frequencyInterval'])
                ->setCycles("0")
                ->setAmount(new Currency(array('value' => $resource['amount'], 'currency' => $resource['currency'])));
// Charge Models
//		$chargeModel = new ChargeModel();
//		$chargeModel->setType('SHIPPING')
//				->setAmount(new Currency(array('value' => 10, 'currency' => $resource['currency'])));
//		$paymentDefinition->setChargeModels(array($chargeModel));




        $merchantPreferences = new MerchantPreferences();
// ReturnURL and CancelURL are not required and used when creating billing agreement with payment_method as "credit_card".
// However, it is generally a good idea to set these values, in case you plan to create billing agreements which accepts "paypal" as payment_method.
// This will keep your plan compatible with both the possible scenarios on how it is being used in agreement.
        $merchantPreferences->setReturnUrl($routeReturnUrl)
                ->setCancelUrl($routeCancelUrl)
                ->setAutoBillAmount("yes")
                ->setInitialFailAmountAction("CONTINUE")
                ->setMaxFailAttempts("0")
                ->setSetupFee(new Currency(array('value' => $resource['amount'], 'currency' => $resource['currency'])));
        $plan->setPaymentDefinitions(array($paymentDefinition));
        $plan->setMerchantPreferences($merchantPreferences);




        //create plan
        try {
            $createdPlan = $plan->create($this->apiContext);


            $patch = new Patch();
            $value = new PayPalModel('{"state":"ACTIVE"}');
            $patch->setOp('replace')
                    ->setPath('/')
                    ->setValue($value);
            $patchRequest = new PatchRequest();
            $patchRequest->addPatch($patch);
            $createdPlan->update($patchRequest, $this->apiContext);

            return $createdPlan;
        } catch (PayPal\Exception\PayPalConnectionException $ex) {
            return back()->with([
                        'error' => trans('mensajes.operacion.incorrecta'),
            ]);
        }
    }

    private function agreementProcess($resource, $idPlan) {




        $current_time = \Carbon\Carbon::now('America/Mexico_City')
                ->addMonths($resource['frequencyInterval'])
                ->toRfc3339String();




        // Create new agreement
        $agreement = new Agreement();
        $agreement->setName($resource['title'])
                ->setDescription($resource['description'])
                ->setStartDate($current_time);
// Set plan id
        $plan = new Plan();
        $plan->setId($idPlan);
        $agreement->setPlan($plan);

// Add payer type
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $agreement->setPayer($payer);



        try {
            // Create agreement
            return $agreement->create($this->apiContext);
        } catch (PayPal\Exception\PayPalConnectionException $ex) {
            return back()->with([
                        'error' => trans('mensajes.operacion.incorrecta'),
            ]);
        }
    }

    public function getResponsePlan(Request $request, $id, $success) {

        $input = $request->all();

        $classPaypal = $this->classPaypal;

        $pago = $classPaypal::findOrFail($id);
        
        if ($success == '0') {
            $pago->response = 'canceled';
            $pago->token = $input['token'];
            $pago->save();

            $params = array_merge(['id' => $pago->id], $this->optionalParams);
            return redirect()->route($this->routeDetail, $params);
        }


        if (empty($input['token'])) {

            $pago->response = 'failed';
            $pago->inputLog = $input;
            $pago->save();

            $params = array_merge(['id' => $pago->id], $this->optionalParams);
            return redirect()->route($this->routeDetail, $params);
        }


        $createdAgreement = new Agreement();


        try {
            // Execute agreement
            $agreement = $createdAgreement->execute($input['token'], $this->apiContext);
        } catch (PayPal\Exception\PayPalConnectionException $ex) {
            $pago->response = 'failed';
            $pago->errorLog = $ex->getData();
            $pago->save();
            $params = array_merge(['id' => $pago->id], $this->optionalParams);
            return redirect()->route($this->routeDetail, $params);
        }
//dd($agreement, $pago);

        if ($pago->response == 'incomplete') {


            $pago->response = 'approved';
            $pago->referenceAgreementId = $agreement->getId();

            $pago->isDirect = FALSE;
            $pago->state = $agreement->getState();

            $payerInfo = $agreement->getPayer()->getPayerInfo()->toArray();

            if (isset($payerInfo['email'])) {
                $pago->payerEmail = $payerInfo['email'];
            }
            if (isset($payerInfo['payer_id'])) {
                $pago->payerId = $payerInfo['payer_id'];
            }

            $pago->payerInfo = $payerInfo;
            $pago->data = $agreement->toArray();

            $pago->fill($input);
            $pago->save();

            $plan = $pago->paypalPlan;

            $plan->referenceAgreementId = $agreement->getId();
            $plan->save();
        }

        $params = array_merge(['id' => $pago->id], $this->optionalParams);
        return redirect()->route($this->routeDetail, $params);
    }

}
