<?php

namespace Webdecero\Paypal\Manager\Models;

use Webdecero\Base\Manager\Models\Base;

class PaypalLog extends Base {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $collection = 'paypalLog';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['type', 'data'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public function user() {
        
        // setup PayPal api context
        $configPaypalUser = config('manager.paypal.userRelationships');
            
		return $this->belongsTo($configPaypalUser);
    }

    public function paypalPlan() {
        return $this->belongsTo('Webdecero\Paypal\Manager\Models\PaypalPlan');
    }

    public function paypal() {
        return $this->belongsTo('Webdecero\Paypal\Manager\Models\Paypal');
    }

}
