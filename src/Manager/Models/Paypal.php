<?php

namespace Webdecero\Paypal\Manager\Models;

use Webdecero\Base\Manager\Models\Base;

class Paypal extends Base {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $collection = 'paypal';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'title',
		'description',
		'_id',
		'reference',
		'response',
		'type',
		'context',
		'price',
		'amount',
		'currency',
		'approvalUrl',
		'discounts',
		'isDirect',
		'voucher',
		'data',
		
		'phone',
		'address',
		'colony',
		'delegation',
		'cp',
		
		
		
		'paymentId',
		'token',
		'PayerID',
		
		'state',
		'frequency',
		'frequencyInterval',
		'paymentPeriod',
		

	];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	public function user() {
            
        // setup PayPal api context
        $configPaypalUser = config('manager.paypal.userRelationships');
            
		return $this->belongsTo($configPaypalUser);
	}

	public function paypalPlan() {
		return $this->belongsTo('Webdecero\Paypal\Manager\Models\PaypalPlan');
	}
        
        
       public function paypalLogs() {
		return $this->hasMany('Webdecero\Paypal\Manager\Models\PaypalLog');
	}
        
        public function getAmountFormatAttribute($value) {


		return '$' . number_format($this->attributes['amount'], 0, '.', ',').$this->attributes['currency'] ;
	}


}
