<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | This file is where you may define all of the routes that are handled
  | by your application. Just tell Laravel the URIs it should respond
  | to using a Closure or controller method. Build something great!
  |
 */

//paypal
Route::get('paypal/detail/{id}', ['as' => 'paypal.detail', 'uses' => 'PageController@payDetail']);
Route::get('paypal/reply/{id}', ['as' => 'paypal.reply', 'uses' => 'PageController@payReply']);

Route::post('paypal', ['as' => 'paypal.payment', 'uses' => 'PayController@postPayment']);
Route::post('paypal/registry', ['as' => 'paypal.registry', 'uses' => 'PayController@postUserRegitry']);

Route::get('paypal/response/payment/{id}/{success}', ['as' => 'paypal.response.payment', 'uses' => 'PayController@getResponsePayment']);
Route::get('paypal/response/plan/{id}/{success}', ['as' => 'paypal.response.plan', 'uses' => 'PayController@getResponsePlan']);
