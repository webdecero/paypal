<?php

namespace Webdecero\Paypal\Pages\Traits;

//Models
//Helpers and Class
use Illuminate\Http\Request;
//Context Paypal
//Unique transaccion
use PayPal\Api\Amount;
//use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
//use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;

//use PayPal\Api\ChargeModel;




trait PaypalPayments {

    private function paymentProcess($resource, $routeReturnUrl, $routeCancelUrl) {


        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

//		$paypalListItem = [];
//		// Set payment amount
//$amount = new Amount();
//$amount->setCurrency($resource['currency'])
//  ->setTotal(10);
//		$item = new Item();
//		$item->setName($resource['title']) // item name
//				->setSku($resource['_id'])
//				->setCurrency($resource['currency'])
//				->setQuantity(1)
//				->setPrice($resource['amount']); // unit price
//
//		$paypalListItem [] = $item;
        // add item to list
//		$item_list = new ItemList();
//		$item_list->setItems($paypalListItem);

        $amount = new Amount();
        $amount->setCurrency($resource['currency'])
                ->setTotal($resource['amount']);

        $transaction = new Transaction();
        $transaction->setAmount($amount);


        if (isset($resource['itemList']) && !empty($resource['itemList'])) {

            $item_list = new ItemList();
            $item_list->setItems($resource['itemList']);

            $transaction->setItemList($item_list);
        }



        $transaction->setDescription($resource['description'])
                ->setInvoiceNumber($resource['_id']);



        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl($routeReturnUrl)
                ->setCancelUrl($routeCancelUrl);

        $payment = new Payment();
        $payment->setIntent('Sale')
                ->setPayer($payer)
                ->setRedirectUrls($redirect_urls)
                ->setTransactions(array($transaction));

        try {
            return $payment->create($this->apiContext);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            return back()->with([
                        'error' => trans('mensajes.operacion.incorrecta'),
            ]);
        }
    }

    public function getResponsePayment(Request $request, $id, $success) {


        $input = $request->all();

        $classPaypal = $this->classPaypal;

        $pago = $classPaypal::findOrFail($id);



        if ($success == '0') {
            $pago->response = 'canceled';
            $pago->token = $input['token'];
            $pago->save();

            $params = array_merge(['id' => $pago->id], $this->optionalParams);
            return redirect()->route($this->routeDetail, $params);
        }


        if (empty($input['PayerID']) || empty($input['token']) || empty($input['paymentId']) || $input['paymentId'] != $pago->paymentId) {

            $pago->response = 'failed';

            $pago->inputLog = $input;

            $pago->save();

            $params = array_merge(['id' => $pago->id], $this->optionalParams);
            return redirect()->route($this->routeDetail, $params);
        }



        $payment = Payment::get($input['paymentId'], $this->apiContext);
        $state = $payment->getState();

//dd($payment);



        if ($state == 'created') {

            try {


                $execution = new PaymentExecution();
                $execution->setPayerId($input['PayerID']);

                //Execute the payment
                $payment = $payment->execute($execution, $this->apiContext);
            } catch (\PayPal\Exception\PayPalConnectionException $ex) {

                $pago->response = 'failed';
                $pago->errorLog = $ex->getData();
                $pago->save();

                $params = array_merge(['id' => $pago->id], $this->optionalParams);
                return redirect()->route($this->routeDetail, $params);
            }
        }


        if ($pago->response == 'incomplete') {

            $pago->isDirect = FALSE;
            $pago->response = $payment->getState();



            $payerInfo = $payment->getPayer()->getPayerInfo()->toArray();

            if (isset($payerInfo['email'])) {
                $pago->payerEmail = $payerInfo['email'];
            }
            if (isset($payerInfo['payer_id'])) {
                $pago->payerId = $payerInfo['payer_id'];
            }



            $pago->payerInfo = $payerInfo;
            $pago->data = $payment->toArray();

            $pago->fill($input);
            $pago->save();
        }

        $params = array_merge(['id' => $pago->id], $this->optionalParams);

        return redirect()->route($this->routeDetail, $params);
    }

}
