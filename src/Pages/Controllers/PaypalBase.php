<?php

namespace Webdecero\Paypal\Pages\Controllers;

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use Webdecero\Base\Pages\Controllers\PagesController as WebdeceroPageController;

class PaypalBase extends WebdeceroPageController {

    protected $apiContext;
    protected $classPaypal = \Webdecero\Paypal\Manager\Models\Paypal::class;
    protected $classPaypalPlan = \Webdecero\Paypal\Manager\Models\PaypalPlan::class;
    protected $routeDetail = 'page.paypal.detail';
    protected $optionalParams = [];
    

    public function __construct() {

        parent::__construct();
        // setup PayPal api context
        $configPaypal = config('manager.paypal');

        $this->apiContext = new ApiContext(new OAuthTokenCredential($configPaypal['client_id'], $configPaypal['secret']));
        $this->apiContext->setConfig($configPaypal['settings']);
    }

}
