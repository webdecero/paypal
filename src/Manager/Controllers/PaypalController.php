<?php

namespace Webdecero\Paypal\Manager\Controllers;

//Providers
use Auth;
//Models
//Helpers and Class
use Webdecero\Base\Manager\Controllers\ManagerController;

class PaypalController extends ManagerController {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {


        $this->data['user'] = Auth::user();

        return view('baseViews::paypalPanel', $this->data);
    }

   

}
