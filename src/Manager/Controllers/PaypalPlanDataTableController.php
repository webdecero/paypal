<?php

namespace Webdecero\Paypal\Manager\Controllers;

//Models

use Webdecero\Paypal\Manager\Models\PaypalPlan;
//Helpers and Class
use Webdecero\Base\Manager\Controllers\DataTableController;

class PaypalPlanDataTableController extends DataTableController {

    protected $collectionName = 'paypalPlan';
	protected $searchable = [
		'query' => [
			'_id' => 'contains',
			'reference' => 'contains',
			'referenceAgreementId' => 'contains',
			'title' => 'contains',
			'user_id' => 'contains'
		],
		'paymentPeriod' => 'equal',
		'state' => 'equal',
	];
	protected $fieldsDetails = [
		'_id' => 'ID',
		'reference' => 'Referencia',
		'referenceAgreementId' => 'Referencia Pagos',
		'user' => 'Usuario',
		'state' => 'Estatus',
		'type' => 'Tipo',
		'amountFormat' => 'Total',
		'currency' => 'moneda',
		'title' => 'Titulo',
		'description' => 'Descripción',
		'context' => 'Contexto',
		"paymentPeriod" => "Periodo",
		"frequencyInterval" => "Intervalo",
		'updated_at' => 'Fecha Actualización',
		'created_at' => 'Fecha Creación',
	];
	protected $fieldsDetailsExcel = [
		'_id' => 'ID',
		'reference' => 'Referencia',
		'referenceAgreementId' => 'Referencia Pagos',
		'user' => 'Usuario',
		'state' => 'Estatus',
		'type' => 'Tipo',
		'amountFormat' => 'Total',
		'currency' => 'moneda',
		'title' => 'Titulo',
		'description' => 'Descripción',
		'context' => 'Contexto',
		"paymentPeriod" => "Periodo",
		"frequencyInterval" => "Intervalo",
		'updated_at' => 'Fecha Actualización',
		'created_at' => 'Fecha Creación',
	];

	protected function formatRecord($field, $item) {

		$record = parent::formatRecord($field, $item);


		if ($field == 'user') {

			$pago = PaypalPlan::find((string) $item['_id']);

			$record = isset($pago->user->email) ? $pago->user->email : 'ninguno';
		} else if ($field == 'amountFormat') {

			$record = '$' . number_format($item['amount'], 0, '.', ',');
		} else if ($field == 'referenceAgreementId') {

			if (isset($item['referenceAgreementId'])) {
				$record = "<a href=" . route('manager.paypal.index', ['s' => $item['referenceAgreementId']]) . "  class='text-center center-block' data-toggle='tooltip' title='Ver pagos relacionados' > {$item['referenceAgreementId'] } </a>";
			} else {
				$record = 'ninguno';
			}
		} else if ($field == 'url_details') {

			$record = route('manager.paypal.plan.dataTable', ['id' => $item['_id']]);
		} else if ($field == 'opciones') {
			
		}

		return $record;
	}

}
