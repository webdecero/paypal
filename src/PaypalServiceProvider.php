<?php

namespace Webdecero\Paypal;

use Illuminate\Support\ServiceProvider;
use RuntimeException;
use Illuminate\Support\Facades\Route;

//use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class PaypalServiceProvider extends ServiceProvider {

    private $configPaypalManager = __DIR__ . '/../config/manager/paypal.php';
    private $folderViewsManager = __DIR__ . '/Manager/resources/views';

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() {


        $this->bootManager();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {




//        $this->mergeConfigFrom($this->configPaypalManager, 'manager.paypal');

//        $this->app->make('Webdecero\Paypal\Manager\Controllers\PaypalController');
    }

    private function bootManager() {

//        Publishes Configuración Base
//        php artisan vendor:publish --provider="Webdecero\Paypal\PaypalServiceProvider" --tag=config

        $this->publishes([
            $this->configPaypalManager => config_path('manager/paypal.php'),
                ], 'config');

//        Registra Views
        $this->loadViewsFrom($this->folderViewsManager, 'baseViews');

//ROUTE Paypal 
        $config = $this->app['config']->get('manager.base.manager.route', []);

        if (empty($config)) {
            throw new RuntimeException('No se enecontro la configuración Base para ruta');
        }

        $config['namespace'] = 'Webdecero\Paypal\Manager\Controllers';

        $originalMiddleware = $config['middleware'];


        $config['middleware'] = array_merge($originalMiddleware, ['authManager', 'editRoute']);

        Route::group($config, function () {
//        Load routes form file
            $this->loadRoutesFrom(__DIR__ . '/Manager/routes/paypal.php');
        });
    }
    
    
//    private function bootPages() {
//
////        Publishes Configuración Paypal
////        php artisan vendor:publish --provider="Webdecero\Paypal\PaypalServiceProvider" --tag=config
//
//        $this->publishes([
//            $this->configPaypalManager => config_path('pages/paypal.php'),
//                ], 'config');
//
////        Registra Views
//        $this->loadViewsFrom($this->folderViewsManager, 'baseViews');
//
////ROUTE Paypal 
//        $config = $this->app['config']->get('manager.base.manager.route', []);
//
//        if (empty($config)) {
//            throw new RuntimeException('No se enecontro la configuración Base para ruta');
//        }
//
//        $config['namespace'] = 'Webdecero\Paypal\Manager\Controllers';
//
//        $originalMiddleware = $config['middleware'];
//
//
//        $config['middleware'] = array_merge($originalMiddleware, ['authManager', 'editRoute']);
//
//        Route::group($config, function () {
////        Load routes form file
//            $this->loadRoutesFrom(__DIR__ . '/Manager/routes/paypal.php');
//        });
//    }

}
