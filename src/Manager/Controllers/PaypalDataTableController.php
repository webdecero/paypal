<?php

namespace Webdecero\Paypal\Manager\Controllers;

//Models

use Webdecero\Paypal\Manager\Models\Paypal;
//Helpers and Class
use Webdecero\Base\Manager\Controllers\DataTableController;

class PaypalDataTableController extends DataTableController {

    public function __construct() {


        $configPaypal = config('pages.paypal');

        $this->isLive = ($configPaypal['settings']['mode'] == 'live') ? true : false;
    }

    protected $collectionName = 'paypal';
    protected $searchable = [
        'query' => [
            '_id' => 'contains',
            'reference' => 'contains',
            'paymentId' => 'contains',
            'referenceAgreementId' => 'contains',
            'title' => 'contains',
            'user_id' => 'contains',
            'payerEmail' => 'contains',
            'payerId' => 'contains'
        ],
        'response' => 'equal',
        'type' => 'equal',
    ];
    protected $fieldsDetails = [
        '_id' => 'ID',
        'reference' => 'Referencia',
        'referenceId' => 'Referencia Externa',
        'user' => 'Usuario',
        'payerId' => 'IdUsuario paypal',
        'payerEmail' => 'Email Paypal',
        'response' => 'response',
        'type' => 'Tipo',
        'transactionFee' => 'Comisión',
        'amountFormat' => 'Total',
        'currency' => 'moneda',
        'title' => 'Titulo',
        'description' => 'Descripción',
        'context' => 'Contexto',
        "paymentPeriod" => "Periodo",
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación',
    ];
    protected $fieldsDetailsExcel = [
        '_id' => 'ID',
        'reference' => 'Referencia',
        'referenceId' => 'Referencia Externa',
        'user' => 'Usuario',
        'payerId' => 'IdUsuario paypal',
        'payerEmail' => 'Email Paypal',
        'response' => 'response',
        'type' => 'tipo',
        'transactionFee' => 'Comisión',
        'amountFormat' => 'Total',
        'currency' => 'moneda',
        'title' => 'Titulo',
        'description' => 'Descripción',
        'context' => 'Contexto',
        "paymentPeriod" => "Periodo",
        'updated_at' => 'Fecha Actualización',
        'created_at' => 'Fecha Creación',
    ];

    protected function formatRecord($field, $item) {

        $record = parent::formatRecord($field, $item);

        if ($field == 'user') {

            $pago = Paypal::find((string) $item['_id']);

            if (isset($pago->user->email)) {
                $record = "<a href=" . route('manager.user.index', ['s' => $pago->user->email]) . "  class='text-center center-block' data-toggle='tooltip' title='Ver usuario' > {$pago->user->email} </a>";
            } else {
                $record = 'ninguno';
            }
        } else if ($field == 'reference') {

            $record = isset($item['reference']) ? $item['reference'] : 'ninguno';
        } else if ($field == 'transactionFee') {

            $record = isset($item['transactionFee']) ? '$' . number_format($item['transactionFee'], 2, '.', ',') : 'ninguno';
        } else if ($field == 'amountFormat') {

            $record = isset($item['amount']) ? '$' . number_format($item['amount'], 2, '.', ',') : 'ninguno';
        } else if ($field == 'referenceId') {

            $record = '';
            if ($item['type'] == 'Pay') {
                $record = isset($item['paymentId']) ? $item['paymentId'] : 'ninguno';
            } else {
                $record = isset($item['referenceAgreementId']) ? $item['referenceAgreementId'] : 'ninguno';
            }
        } else if ($field == 'payerId') {


            if (isset($item['payerId'])) {
                $record = "<a href=" . route('manager.user.index', ['s' => $item['payerId']]) . "  class='text-center center-block' data-toggle='tooltip' title='Ver usuarios paypal' > {$item['payerId'] } </a>";
            } else {
                $record = 'ninguno';
            }
        } else if ($field == 'url_details') {

            $record = route('manager.paypal.dataTable', ['id' => $item['_id']]);
        } else if ($field == 'opciones') {


            if ($this->isLive) {
                $link = 'https://www.paypal.com/webscr?cmd=_history-details-from-hub&id=';
            } else {
                $link = 'https://www.sandbox.paypal.com/webscr?cmd=_history-details-from-hub&id=';
            }

            if (isset($item['reference'])) {
                $record = "<a target='_blank' href=" . $link . $item['reference'] . "  class='text-center center-block' data-toggle='tooltip' title='Ver Detalle paypal' > <i class='fa fa-eye'></i> </a>";
            }


        }

        return $record;
    }

    protected function formatRecordExcel($field, $item) {


        $record = $this->formatRecord($field, $item);


        if ($field == 'user') {

            $pago = Paypal::find((string) $item['_id']);

            $record = isset($pago->user->email) ? $pago->user->email : 'ninguno';
        } else if ($field == 'payerId') {


            $record = isset($item['payerId']) ? $item['payerId'] : 'ninguno';
        }





        return $record;
    }

}
