<?php

namespace Webdecero\Paypal\Pages\Controllers;
//Providers
//use Validator;
//Models
use Webdecero\Paypal\Manager\Models\Paypal;
use Webdecero\Paypal\Manager\Models\PaypalPlan;
use Webdecero\Paypal\Manager\Models\PaypalLog;
//Helpers and Class
use Illuminate\Http\Request;

use Webdecero\Base\Pages\Controllers\PagesController as WebdeceroPageController;
class PaypalWebhook extends WebdeceroPageController {

    protected function index(Request $request) {

//		$sale = '';
		$input = $request->all();

//		$input = json_decode($sale, true);
//		$erro = json_last_error_msg();



		$response = false;
		$resourceType = $input['resource_type'];
		$eventType = $input['event_type'];



		try {


			$log = new PaypalLog();
			$log->response = $input;
			$log->resourceType = $resourceType;
			$log->eventType = $eventType;
			$log->save();

			switch ($resourceType) {
				case 'Agreement':

					$response = $this->parseAgreement($log);

					break;

				case 'sale':
					$response = $this->parseSale($log);

					break;
			}
			
			$log->isExecuteCorrect = $response;
			
		} catch (\Exception $e) {

			$log->isExecuteCorrect = false;
			$log->errorMessage = $e->getMessage();
			
			
			
		} finally {
			$log->save();
		}



		
	}

	protected function parseSale(PaypalLog $log) {
		$response = false;

		switch ($log->eventType) {
			case 'PAYMENT.SALE.COMPLETED':
				$resource = $log->response['resource'];
				if (isset($resource['billing_agreement_id'])) {

					$response = $this->planSaleCompleted($log);
				} else if (isset($resource['parent_payment'])) {
					$response = $this->paymentSaleCompleted($log);
				}
				break;
		}



		return $response;
	}

	protected function planSaleCompleted(PaypalLog $log) {


		$response = false;
		$resource = $log->response['resource'];

		$billingAgreementId = $resource['billing_agreement_id'];
		$idPayment = $resource['id'];

//Existe una referencia previa  significa que es el fee inicial
		$payReferenceExist = Paypal::where('referenceAgreementId', $billingAgreementId)->where('reference', $idPayment)->first();


		if (empty($payReferenceExist)) {

			$pay = Paypal::where('referenceAgreementId', $billingAgreementId)->first();
			$response = $this->planApprovePay($log, $pay);
		} else {
			$pay = Paypal::where('referenceAgreementId', $billingAgreementId)->where('isSetupFee', true)->first();
			$response = $this->planCreatePay($log, $pay);
		}

		return $response;
	}

	protected function planApprovePay(PaypalLog $log, Paypal $pay) {


		$resource = $log->response['resource'];

		$idPayment = $resource['id'];

		$user = $pay->user;
		$paypalPlan = $pay->paypalPlan;

		if (!empty($user)) {
			$user->paypalLogs()->save($log);
		}

		$paypalPlan->paypalLogs()->save($log);

		$pay->reference = $idPayment;
		$pay->transactionFee = $resource['transaction_fee']['value'];
		$pay->isWebhookValide = true;
		$pay->isSetupFee = true;

		$pay->paypalLogs()->save($log);
		$pay->save();

		$log->validePayment = true;
		$log->save();
		return true;
	}

	protected function planCreatePay(PaypalLog $log, Paypal $payParent) {


		$pay = $payParent->replicate();

		$resource = $log->response['resource'];
		$idPayment = $resource['id'];

		$pay->payParentId = (string) $payParent->_id;
		$pay->reference = $idPayment;
		$pay->transactionFee = isset($resource['transaction_fee']['value']) ? $resource['transaction_fee']['value'] : '';
		$pay->isWebhookValide = true;
		$pay->isSetupFee = false;


		$pay->save();

		$pay->paypalLogs()->save($log);


		$user = $pay->user;
		$paypalPlan = $pay->paypalPlan;


		$log->validePayment = true;
		$log->save();

		if (!empty($user)) {
			$user->paypalLogs()->save($log);
		}
		$paypalPlan->paypalLogs()->save($log);


		return true;
	}

	protected function paymentSaleCompleted(PaypalLog $log) {

		$resource = $log->response['resource'];

		$parentPayment = $resource['parent_payment'];
		$idPayment = $resource['id'];

		$pay = Paypal::where('paymentId', $parentPayment)->first();

		if (!empty($pay) && !empty($resource['parent_payment'])) {

			$user = $pay->user;

			if (!empty($user)) {
				$user->paypalLogs()->save($log);
			}

			$pay->reference = $idPayment;
			$pay->transactionFee = $resource['transaction_fee']['value'];
			$pay->isWebhookValide = true;

			$pay->paypalLogs()->save($log);
			$pay->save();

			$log->validePayment = true;
			$log->save();

			return true;
		}
		return false;
	}

	protected function parseAgreement(PaypalLog $log) {

		$response = false;
		switch ($log->eventType) {
			case 'BILLING.SUBSCRIPTION.CREATED':

				$response = $this->billingCreated($log);

				break;


			case 'BILLING.SUBSCRIPTION.CANCELLED':

				$response = $this->billingCancelled($log);

				break;
		}

		return $response;
	}

	protected function billingCancelled(PaypalLog $log) {

		$resource = $log->response['resource'];

		$id = $resource['id'];

		$paypalPlan = PaypalPlan::where('referenceAgreementId', $id)->first();

		if (!empty($paypalPlan)) {

			$state = strtoupper($paypalPlan->state);

			if ($state == 'CANCELED') {

				$paypalPlan->state = $resource['state'];
			}
			$paypalPlan->isWebhookValide = true;

			$paypalPlan->paypalLogs()->save($log);

			$paypalPlan->save();

			return true;
		}

		return false;
	}

	protected function billingCreated(PaypalLog $log) {

		$resource = $log->response['resource'];

		$id = $resource['id'];

		$paypalPlan = PaypalPlan::where('referenceAgreementId', $id)->first();

		if (!empty($paypalPlan)) {

			if ($paypalPlan->state == 'CREATED') {
				$paypalPlan->state = $resource['state'];
			}

			$paypalPlan->isWebhookValide = true;

			$paypalPlan->paypalLogs()->save($log);

			$paypalPlan->save();

			return true;
		}

		return false;
	}

}
