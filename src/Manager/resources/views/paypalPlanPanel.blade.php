@extends('baseViews::layouts.dataTable')

@section('sectionAction', 'Tabla')
@section('sectionName', 'Planes Paypal')
@section('formAction', route('manager.paypal.plan.dataTable'))


@section('inputs')

<div class="form-group">
	<label for="state">Estatus</label> <br>

	<select class="reloadTable" name="state">
		<option value="">Ninguno</option>
		<option value="Created">Creado</option>
		<option value="Active">Activo</option>
		<option value="Canceled">Cancelado</option>


	</select>
</div>


<div class="form-group">
	<label for="paymentPeriod">Periodo</label> <br>

	<select class="reloadTable" name="paymentPeriod">
		<option value="">Ninguno</option>
		<option value="mensual">Mensual</option>
		<option value="trimestral">Trimestral</option>
		<option value="anual">Anual</option>


	</select>
</div>


<div class="btn-toolbar pull-right" role="toolbar" aria-label="...">


	<div class="btn-group" role="group" aria-label="...">
		<button type="submit"class="btn btn-success " >

			<i class="fa fa-file-excel-o"></i> EXPORT EXCEL

		</button>
	</div>
</div>


@endsection



@section('dataTableColums')


<th data-details="true" ></th>


<th data-name="_id" >
	<strong>Id</strong>
</th>

<th data-name="title" >
	<strong>Titulo</strong>
</th>



<th data-name="referenceAgreementId"  data-orderable="true"  data-order="asc" >
	<strong>Referencia Pagos</strong>
</th>



<th data-name="paymentPeriod"  data-orderable="true"  data-order="asc" >
	<strong>Periodo</strong>
</th>


<th data-name="amountFormat" data-orderable="true">
	<strong>Total </strong>
</th>


<th data-name="state" data-orderable="true" >
	<strong>Estatus</strong>
</th>


<th data-name="user" >
	<strong>Usuario</strong>
</th>

<th data-name="created_at"><strong>Fecha Alta</strong></th>

<!--<th data-name="opciones">
	<strong>Opciones</strong>
</th>-->




@stop
