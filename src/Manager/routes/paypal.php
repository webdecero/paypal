<?php

//Paypal
Route::post('paypal/dataTable/{id?}/{excel?}', array('as' => 'manager.paypal.dataTable', 'uses' => 'PaypalDataTableController@dataTable'));
Route::resource('paypal', 'PaypalController', [
    'only' => [
        'index'
    ],
    'names' => [
        'index' => 'manager.paypal.index',
        'create' => 'manager.paypal.create',
        'store' => 'manager.paypal.store',
        'show' => 'manager.paypal.show',
        'edit' => 'manager.paypal.edit',
        'update' => 'manager.paypal.update',
        'destroy' => 'manager.paypal.destroy',
    ]
]);

//Plan Paypal
Route::post('paypal/plan/dataTable/{id?}/{excel?}', array('as' => 'manager.paypal.plan.dataTable', 'uses' => 'PaypalPlanDataTableController@dataTable'));
Route::resource('paypal/plan', 'PaypalPlanController', [
    'only' => [
        'index'
    ],
    'names' => [
        'index' => 'manager.paypal.plan.index',
        'create' => 'manager.paypal.plan.create',
        'store' => 'manager.paypal.plan.store',
        'show' => 'manager.paypal.plan.show',
        'edit' => 'manager.paypal.plan.edit',
        'update' => 'manager.paypal.plan.update',
        'destroy' => 'manager.paypal.plan.destroy',
    ]
]);
